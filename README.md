# Build status

[![build status](https://gitlab.coko.foundation/pubsweet/pubsweet-client/badges/master/build.svg)](https://gitlab.coko.foundation/pubsweet/pubsweet-client/builds)

# Description

This is the PubSweet client, to be used as a dependency in PubSweet apps, such as: https://gitlab.coko.foundation/pubsweet/science-blogger

# Short-term roadmap

- 1.0.0.alpha.1 - Current version
- TBD after first PubSweet meeting on July 18, 2017
